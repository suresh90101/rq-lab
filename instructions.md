---
author: #module_leader
date: #date
documentclass: hitec
fontfamily: opensans
fontsize: 12
...
# Overview

This practical focuses on the Research Questions component of the
coursework.

In addition to the research question itself, the research question
deliverable needs to identify all the components of a research
question, as discussed in the Literature Reviews lecture:

1. Context.
2. Population.
3. Intervention.
4. Comparison.
5. Outcome.

The research question deliverable must be submitted in
[YAML](https://en.wikipedia.org/wiki/YAML) format.  YAML is a data serialization language that
uses key-value pairs, similar to those in Windows "INI" files.  The
syntax is useful for "semi-structured" text, but it is a formal
language with a well-defined syntax that must be adhered to.

You will submit your research questions using Git, so it
is important that you understand how to use Git to commit and upload
files to Bitbucket from the command line.  Using the Bitbucket web
interface is not acceptable and will result in reduced marks for the
"Research Infrastructure" component  of the coursework.

# Instructions

#. Read Section 5.3 "The Research Question(s)" of Kitchenham &
 Charters' "Guidelines for performing systematic literature
                  reviews in software engineering".

#. Read the "Research questions" section of the  coursework
 specification, to be sure you understand the requirements for this
 part of the coursework.

#. Discuss potential research topics among yourselves.  You _must_
 choose a topic related to computer science, software engineering, robotics, artificial
intelligence and machine learning, cybersecurity, and data science.

#. Decide on a topic, then formulate a general research question.

#. Refine your general research question into one (1)
 specific research question that can be answered by a _systematic
 literature review_.

    Note: the "dataset" you will use to answer your research question
    is the set of papers in the IEEE Digital Library.  As such, your
    question should not require an experiment or statistical analysis,
    as this dataset is unlikely to support such a question.

#. If you haven't already done so, clone your group's Bitbucket repository.

#. Copy #filename(research_question.yml) from this workspace to your group workspace.

#. Change to your group workspace, and open #filename(research_question.yml)
 (using Notepad++ or your favorite text editor, but **NOT** windows Notepad!).

#. Write your group name as it appears on Canvas.

#. Write the names (but **NOT** student IDs) of each group member 
 after `members: `.  Be sure to format this field as a comma-separated
 list enclosed in square brackets.

#. Write the general topic area from which you derived your research question.

    _Start with a pretend topic on the first iteration; you need to
    understand how to create and validated YAML first, before you
    elaborate your topic to something suitable for your group and this deliverable._
    
#. Write your research question after.  Be sure it is an actual
 _question_ and contains all the components of a research question.

    _Start with a pretend question on the first iteration; you need to
    understand how to create and validated YAML first, before you
    elaborate your topic to something suitable for your group and this deliverable._


#. Identify and document the:

    * context,
    * population,
    * intervention,
    * comparison,
    * outcomes.

    Refer to the Kitchenham & Charters guidelines and the lecture notes for an explanation of these terms.

    _Start with pretend components on the first iteration; you need to
    understand how to create and validated YAML first, before you
    elaborate your topic to something suitable for your group and this deliverable._


#. _Spellcheck^[Notepad has a spellcheck plugin; install and _use_ it.]_, _Proofread_, then commit your #filename(research_question.yml).

#. _Validate_ your #filename(research_question.yml) file using
 [YAML Lint](http://www.yamllint.com/).  Be sure _all_ fields are displayed.

#. Push your workspace to Bitbucket by the deadline (23:59 #cw_rq_due).

#. Have another member of your group pull/clone your repository, then
 validate your #filename(research_question.yml) file _again_ using
 [YAML Lint](http://www.yamllint.com/).  Be sure _all_ fields are
 displayed.  This will ensure you did not introduce a syntax error
 during the commit/push steps.

# Example

The following example was proposed during one the practical sessions:

RQ: “Does a higher proportion of women in the population result in higher literacy among women in the developing world?”

Following are the components associated with this question.

* Context: this is the setting or environment in which the question should be interpreted.

    For the above question, the context is _education in the developing world._

* Population: this can be tricky. The population is the group from
  which the sub-groups that will be compared are selected (see the
  Research Design lecture notes for an explanation of sub-groups).

    For questions that involve humans, it’s easier: for our question above, it’s female students.

    But some groups will ask about technology, such as what algorithms produce the best text recognition? In this case, the population is the text that’s being analyzed (such as commit log entries or discussion forum posts).

* Intervention: this is the most difficult.  For obvious cases like
clinical trials, this is the treatment (vaccine, therapy, drug) that
is being tested.  For computer science, this is the algorithm,
technique, tool, etc. that you think might produce a better result.

    My suggestion is to start with the comparison, then ask what will cause there to be a difference between the sub-groups.

    For the question about female students, the “intervention” is the
proportion of women vs men in the sub-group.  So it's not an
intervention in the sense of something we instigate; rather, it's a
distinguishing factor between different sub-groups.

    For the text recognition question, the interventions are different recognition algorithms, which are all applied to the same sub-group in a “cohort” style research design.

* Comparison: this identifies the sub-groups (or “cohort” if there is
  only one).

    For the education question, the comparison is between or among students
in different countries or states.

    For the text recognition question, the comparison is between the
results of applying different algorithms or approaches to the _same group_.

* Outcome: this is the difference the intervention is thought to produce.

    For our education example, this is female literacy. For the text recognition question, the outcome is accuracy: precision, recall, F-statistic, etc.
